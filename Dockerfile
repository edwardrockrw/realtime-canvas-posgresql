# FROM postgres:13.5-bullseye as build
# WORKDIR /tmp

# COPY . .

FROM postgres:13.5
ENV POSTGRES_USER=admin
ENV POSTGRES_PASSWORD=admin
ENV TZ=Asia/Taipei
ARG DB_VER

COPY *.sql /tmp/
COPY *.sh /docker-entrypoint-initdb.d/


