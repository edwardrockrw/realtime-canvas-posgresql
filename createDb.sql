-- public."user" definition
CREATE TABLE public."user" (
	id serial4 NOT NULL,
	email varchar NOT NULL,
	"name" varchar NOT NULL,
	"password" varchar NULL,
	"currentHashedRefreshToken" varchar NULL,
	created_date timestamp NOT NULL DEFAULT now(),
	"isRegisteredWithGoogle" bool NOT NULL DEFAULT false,
	"isEmailConfirmed" bool NOT NULL DEFAULT false,
	CONSTRAINT "PK_user_id" PRIMARY KEY (id),
	CONSTRAINT "UQ_email" UNIQUE (email)
);

INSERT INTO public."user" (email,"name","password","currentHashedRefreshToken",created_date,"isRegisteredWithGoogle","isEmailConfirmed") VALUES	 
    ('test@email.com','user1','$2b$10$t7DDrmdhS51bcaY00mSq4e6Y7K4pOl.o14B6PffBMVzMJpigQjoxy','$2b$10$EQSdMZRTU3QNd82Qtnib6.T4ryZSjGw3qM03iE98WzM6JWBKzGrPy','2022-06-28 13:16:16.575179',false,false),
    ('test2@email.com','user2','$2b$10$t7DDrmdhS51bcaY00mSq4e6Y7K4pOl.o14B6PffBMVzMJpigQjoxy','$2b$10$7KW80ZAUgPVzXHjAjG3FVuOUx83LqUjPmtPODbI8.L3cp9JJlMx.u','2022-06-28 13:16:16.588149',false,false);
	 

-- public.images definition
CREATE TABLE public.images (
	id serial4 NOT NULL,
	image_url varchar NOT NULL,
	x_position int4 NOT NULL,
	y_position int4 NOT NULL,
	created_date timestamp NOT NULL DEFAULT now(),
	user_id int4 NULL,
	CONSTRAINT "PK_image_id" PRIMARY KEY (id)
);

-- public.images foreign keys
ALTER TABLE public.images ADD CONSTRAINT "FK_image_user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id);

INSERT INTO public.images (image_url,x_position,y_position,created_date,user_id) VALUES
	 ('',1049,124,'2022-06-28 13:16:16.603925',2),
	 ('',507,292,'2022-06-28 13:16:16.596538',1);


-- public."comment" definition
CREATE TABLE public."comment" (
	id serial4 NOT NULL,
	"text" varchar NOT NULL,
	created_date timestamp NOT NULL DEFAULT now(),
	marker_id int4 NOT NULL,
	removed_at timestamp NULL,
	user_id int4 NULL,
	CONSTRAINT "PK_comment_id" PRIMARY KEY (id)
);
CREATE INDEX "IDX_marker_id" ON public.comment USING btree (marker_id);
CREATE INDEX "IDX_user_id" ON public.comment USING btree (user_id);


-- public."comment" foreign keys
ALTER TABLE public."comment" ADD CONSTRAINT "FK_comment_user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id);


-- public.marker definition
CREATE TABLE public.marker (
	id serial4 NOT NULL,
	image_id int4 NULL,
	x_position int4 NOT NULL,
	y_position int4 NOT NULL,
	active bool NOT NULL DEFAULT true,
	inactive_action varchar NOT NULL DEFAULT ''::character varying,
	updated_date timestamp NOT NULL DEFAULT now(),
	CONSTRAINT "PK_marker_id" PRIMARY KEY (id)
);