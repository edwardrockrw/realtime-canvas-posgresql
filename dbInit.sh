#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "admin" --dbname "postgres" <<-EOSQL
    CREATE DATABASE "nestjs" WITH OWNER = admin ENCODING = 'UTF8' CONNECTION LIMIT = -1;
EOSQL

psql --username "admin" --dbname "nestjs" -f /tmp/createDb.sql


# add init data
if [ "$ENV_INIT_PROFILE" = "" ];
then
	export ENV_INIT_PROFILE=""
fi

profile="/tmp/InitProfile_$ENV_INIT_PROFILE.sql"
if [ -e "$profile" ];
then
	psql --username "admin" --dbname "nestjs" -f $profile
fi
